# TPass
This is a password generator tool, developed in pure python.

## TO-DO
- implement default option without specifying each set

## DONE
- Function to generate password with N chars
- generate password mixing lower and upper case letters
- generate password mixing letters and numbers
- generate password mixing letters, numbers, and special characters
- mix characters order
