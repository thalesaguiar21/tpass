import random
from typing import List


def generate(lower: int = 3, upper: int = 3, nums: int = 3, nsp: int = 3) -> str:
    lcs = [_randlc() for _ in range(lower)]
    ucs = [_randuc() for _ in range(upper)]
    num = [_randnum() for _ in range(nums)]
    special = [_randc() for _ in range(nsp)]
    pwd = ucs + lcs + num + special
    random.shuffle(pwd)
    return "".join(pwd)


def _random_ascii(a: int = 33, b: int = 126) -> str:
    return chr(random.randint(a, b))


def _randlc() -> str:
    return _random_ascii(97, 122)


def _randuc() -> str:
    return _random_ascii(65, 90)


def _randnum() -> str:
    return _random_ascii(48, 57)


def _randc() -> str:
    special_char = []
    special_char.extend(list(range(33, 47)))
    special_char.extend(list(range(58, 64)))
    special_char.extend(list(range(91, 96)))
    special_char.extend(list(range(123, 126)))
    return chr(random.choice(special_char))


pwd = generate(3, 5, 3, 3)
print(pwd)